# Evercam Gateway Dashboard

This is the publicly accessible dashboard which allows Evercam users to:

*  Claim and approve a Gateway Device
*  View discovered cameras on the LAN
*  Add those cameras to Evercam 

Authentication is via Evercam account sign-in only. 

## Installation

The site is a Node.js application. You can install it as follows:

    git clone https://github.com/evercam/evercam-gateway-dashboard
    cd evercam-gateway-dashboard
    npm install

It currently requires 3 environment variables to be set:

*  SESSION_SECRET: a salt for the session middleware 
*  EVERCAM_API_ID: an Evercam Developer ID (temporary)
*  EVERCAM_API_KEY: am Evercam Developer Key (temporary)

Optionally the following environment variables can also be set:

*  PORT: the port you want the application to run on (default is 443)

The application also relies on an installation of a local Redis instance: http://redis.io/download

Finally the application is designed to run over HTTPS. You will need to create an SSL Certificate and Key and place them in:

    ~/.ssl/gateway_evercam_io.key
    ~/.ssl/gateway_evercam_io.crt

Depending on your cert provider, you may also need to add some intermediate certificates. These can be specified in the `bin/www` file.

## Running

Once installation is complete you can do:

    cd evercam-gateway-dashboard/
    node ./bin/www
