var https = require('https');

var get_token_info = function(access_token, on_success) {

  var response_data = '';

  var options = {
    host: 'auth.evercam.io',
    port: 443,
    path: '/oauth2/tokeninfo?access_token=' + access_token
  };

  https.get(options, function(resp){
    resp.on('data', function(chunk){
      response_data=response_data+chunk;
    });
    resp.on('end', function(){
      on_success(response_data);
    });
  }).on("error", function(e){
    console.log("Got error: " + e.message);
  });

};

exports.get_token_info = get_token_info;
