var VideoTimer = {

  init:function(id){
    this[id] = {
      obj:document.getElementById(id)
    }
  },

  start:function(id){
    var obj = this[id];
    obj.srt = new Date();
    clearTimeout(obj.to);
    this.tick(id);
  },

  stop:function(id){
    clearTimeout(this[id].to);
  },

  tick:function(id){
    this.stop(id);
    var obj = this[id];
    sec = (new Date() - obj.srt)/1000;
    //sec = sec % 60;
    obj.obj.value = sec.toFixed(3) + " secs";
    //console.log(sec);
    obj.to = setTimeout(function() { VideoTimer.tick(id); }, 1000);
  }
}

// http://www.dynamicdrive.com/forums/showthread.php?68194-How-to-make-Javascript-count-up-timer
