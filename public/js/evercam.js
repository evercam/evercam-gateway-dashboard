/*jslint browser: true*/
/*global $, jQuery, alert*/
;(function (exports) {
    'use strict';

    var api_url = "https://api.evercam.io/v1",
        vendors = [];

    function get_vendors(onCompletion) {
        var url = api_url + "/vendors";
        $.getJSON(url, function (data) {
            onCompletion(data.vendors);
        });
    }

    function vendor_compare(a, b) {
        if (a.name < b.name) {
            return -1;
        }
        if (a.name > b.name) {
            return 1;
        }
        return 0;
    }

    function get_vendor(vendor_id) {
        var result;
        vendors.forEach(function (vendor) {
            if (vendor.id === vendor_id) {
                result = vendor;
            }
        });
        return result;
    }

    var evercam = {

        init: function () {
            get_vendors(function(data) { vendors = data; });
        },

        // intended as public method for returning a list of vendors
        vendors: function () {
            return vendors.sort(vendor_compare);
        },

        // intended as public method for returning a specific vendor 
        vendor: function (vendor_id) {
            return get_vendor(vendor_id);
        },

        get_models: function (vendor_id, onCompletion) {
            var url = api_url + "/models?vendor_id=" + vendor_id,
                models = [],
                page = 1,
                get_page_data = function (data) {
                    var pages = data.pages,
                        vendor = {};
                    page = page + 1;
                    models = models.concat(data.models);
                    if (page > pages) {
                        vendor = get_vendor(vendor_id);
                        vendor.models = models;
                        onCompletion(models);
                    } else {
                        $.getJSON(url + "&page=" + page, function (data) {
                            get_page_data(data);
                        });
                    }
                };
            $.getJSON(url, function (data) {
                console.log(data);
                get_page_data(data);
            });
        },

        get_model_url: function (url_type, vendor_id, model_id) {
            var url = '',
                vendor = get_vendor(vendor_id);
            vendor.models.forEach(function (model) {
                if (model.id === model_id) {
                    if (model.defaults.snapshots !== undefined) {
                        url = model.defaults.snapshots[url_type];
                    }
                }
            });
            return url;
        },

        get_model_auth_basic: function (auth_element, vendor_id, model_id) {
            var element = '',
                vendor = get_vendor(vendor_id);
            vendor.models.forEach(function (model) {
                if (model.id === model_id) {
                    if (model.defaults.auth !== undefined) {
                        element = model.defaults.auth.basic[auth_element];
                    }
                }
            });
            return element;
        },

        add_camera: function (camera, evercamToken, onCompletion) {
            console.log("Adding a camera...");
            // temporarily using api key and id of fixed user, while oAuth add a camera is sorted out
            var url = '/cameras';
            $.ajax({
                type: 'POST',
                url: url,
                //headers: { 'Authorization' : evercamToken['token_type'] + ' ' + evercamToken['access_token'] },
                data: JSON.stringify(camera),
                processData: false,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    onCompletion(data);
                }
            });
        }

    };

    exports.evercam = evercam;

}(this));
