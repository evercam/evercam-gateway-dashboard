/*jslint browser: true*/
/*global $, jQuery, alert*/
;(function (exports) {
    'use strict';

    var url_hash = window.location.hash,
        token = {},
        authHandler,
        OAuthHandler;

    // Reads the #fragment of the url into an array of properties
    function read_url_fragment(url_fragment) {
        var fragmentString = url_fragment.substr(1),
            fragment = {},
            fragmentItemStrings = fragmentString.split('&'),
            fragmentItem,
            i;
        for (i in fragmentItemStrings) {
            fragmentItem = fragmentItemStrings[i].split('=');
            if (fragmentItem.length !== 2) {
                continue;
            }
            fragment[fragmentItem[0]] = fragmentItem[1];
        }
        return fragment;
    }

    // read any fragment in url and see if it is a token
    function read_token(url_fragment) {
        console.log("Reading url fragment into array...");
        var result = read_url_fragment(url_fragment);
        if (result.access_token !== undefined) {
            console.log("Returning token...");
            return result;
        }
        console.log("No token detected...");
        return undefined;
    }

    // Gets the user id associated with the oAuth token from Evercam OAuth
    function get_token_info(from_token, onSuccess) {
        $.getJSON("/auth?access_token=" + from_token.access_token, function (data) {
            if (data.userid !== undefined) {
                onSuccess(data);
            }
        });
    }

    // Saves any token into server session so we can keep user logged in
    function save_evercam_user(onSuccess) {
        console.log("Reading token from url");
        var new_token = read_token(url_hash);
        if (new_token !== undefined) {
            console.log("Getting Evercam User Data with Token from URL fragment...");
            get_token_info(new_token, function (token_info) {
                console.log("Saving User data and token info Server-side...");
                // add the token_type from original authorization to the token_info
                token_info.token_type = new_token.token_type;
                $.post("/auth", token_info, function () {
                    console.log("Saved");
                    onSuccess();
                });
            });
        }
    }

    // Retrieves any existing user token from server session
    // TODO: Really we have to try the get_token_info
    // function every time since otherwise we don't know
    // if token is still actually valid - at least until calls
    // start failing
    function retrieve_token(callback) {
        $.getJSON("/auth", function (data) {
            console.log("Retrieving Evercam Token from serverside...");
            token = data;
        }).complete(function () {
            callback(token);
        });
    }

    // Removes any existing token data from server session
    function delete_token(callback) {
        $.ajax({
            url: '/auth',
            type: 'DELETE',
            success: function () {
                callback();
            }
        });
    }

    var evercam_auth = {

        onAuth: function (handler) {
            authHandler = handler;
        },

        onOAuth: function (handler) {
            OAuthHandler = handler;
        },

        authenticate: function () {
            if (url_hash !== "") {
                console.log("Detect hash fragment in url...");
                save_evercam_user(OAuthHandler);
            } else {
                retrieve_token(authHandler);
            }
        },

        // just the actual token key itself
        access_token: function () {
            return token.access_token;
        },

        // the entire set of token information
        token: function () {
            return token;
        },

        logout: function (callback) {
            delete_token(callback);
        }

    };

    exports.evercam_auth = evercam_auth;

}(this));
