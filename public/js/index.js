;(function (exports) {
    'use strict';

    var mac_address_input = $("#mac-address"),
        secret_input = $("#secret"),
        gateway_name_input = $("#gateway-name"),
        add_gateway_action_button = $("#add-gateway-action-button"),
        logout_button = $("#logout-button"),
        evercam_login_container = $("#login-evercam"),
        dashboard_container = $("#gateway-dashboard");

    // Secondary init called once User is authenticated / or not
    function init_evercam(token) {
        if (token.access_token === undefined) {
            console.log("Showing Evercam Login");
            show_evercam_login();
            showLogoutButton(false);
            hide_dashboard();
        } else {
            show_dashboard();
            showLogoutButton(true);
            evercam_gateway.get_gateways(token.access_token, add_gateways);
        }
    }

    function set_initial_states() {
        hide_evercam_login();
        hide_dashboard();
    }

    function set_handlers() {
        evercam_auth.onAuth(init_evercam);
        evercam_auth.onOAuth(redirect_home);
    }

    function bind_ui_actions() {
        // Bind Logout
        logout_button.on("click", function () {
            evercam_auth.logout(redirect_logout);
        });

        // Bind Claim Button
        add_gateway_action_button.on("click", function(event){
            event.preventDefault();
          //  evercam_gateway.claim_gateway(mac_address_input.val(), secret_input.val(), show_approval);
          evercam_gateway.claim_gateway(mac_address_input.val(), secret_input.val(), gateway_name_input.val(), approve_pending_gateway);

            //Hide and reset the modal
            $("#add-gateway-modal").modal("hide");
            $("#add-gateway-form")[0].reset();
        });
    }

    function add_gateways(gateways) {
        if (gateways !== undefined) {
            gateways.forEach(function (gateway) {
                // Get devices and then add them to the list
                evercam_gateway.get_devices(evercam_auth.access_token(), gateway.id,
                    function (devices) {
                        gateway.devices = devices;
                        gateway_list.add(gateway);
                    });
            });
        } else {
            console.log("Failed to load Gateways");
        }
    }

    function show_evercam_login() {
        evercam_login_container.show();
    }

    function hide_evercam_login() {
        evercam_login_container.hide();
    }

    function showLogoutButton(show) {
      if(show) {
        logout_button.show();
      }
      else {
        logout_button.hide();
      }
    }

    function show_dashboard() {
        dashboard_container.show();
    }

    function hide_dashboard() {
        dashboard_container.hide();
    }

    // Approve the pending gateway if the gateway is valid
    function approve_pending_gateway(pendingGateway){
      if(pendingGateway !== undefined) {
        // connect to approval function
        evercam_gateway.approve_gateway(pendingGateway, evercam_auth.access_token(), show_gateway);
      }
      else {
        //Display Failure Message
        alert('Request Failed. Please check MAC Address and Secret.');
      }
    }

    function show_gateway(gateway) {
        if (gateway !== undefined) {
            gateway_list.add(gateway);
        } else {
            alert('Gateway approval failed.');
        }
    }

    function redirect_logout() {
        window.location = 'index.html';
    }

    function redirect_home() {
        window.location = 'index.html';
    }

    var index = {

        init: function () {
            set_initial_states();
            bind_ui_actions();
            set_handlers();
            evercam_auth.authenticate();
        }

    };

    exports.index = index;

}(this));
