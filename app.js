var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var auth = require('./routes/auth');
var cameras = require('./routes/cameras');
var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Setup Session middleware
store_options = {
  host: '127.0.0.1', port: 6379, prefix: 'gateway-session'
};

app.use(session({
  store: new RedisStore(store_options),
  secret: process.env['SESSION_SECRET'],
  resave: false,
  saveUninitialized: true
}));

// Set actual routes
app.use('/auth', auth); 
app.use('/cameras', cameras);
app.use(express.static(path.join(__dirname, 'public')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send(err + '\n' + err.message + '\n' + err.stack);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.sendFile(__dirname + "/public/error.html"); 
});


module.exports = app;
