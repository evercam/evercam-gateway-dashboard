var express = require('express');
var evercam = require('../evercam.js');
var router = express.Router();

/* GET oAuth Token for Evercam etc */
router.get('/', function(req, res, next) {
  var access_token = req.query.access_token;
  // If asking for further token information
  if (access_token) {
    evercam.get_token_info(access_token, 
        function(data) 
        { 
          res.send(data);
        });
  }else{
    var evercam_token = '';
    // otherwise send existing evercam token if any
    if (req.session) { evercam_token = req.session.evercam_token; }
    res.send(evercam_token);
  }
});

/* SET oAuth Token in Session for Evercam */
router.post('/', function(req, res, next) { 
  req.session.evercam_token = req.body;
  res.send('success');
});

// Logout removing all Session data
router.delete('/', function(req, res) {
  req.session.destroy(function() { 
    
  });
  res.send("success");
});

module.exports = router;
