var express = require('express');
var https = require('https');
var querystring = require('querystring');
var router = express.Router();

// Temporary workaround for add a camera
// until oAuth Add a Camera is enabled
router.post('/', function(req, res, next) { 
  var camera = JSON.stringify(req.body),
      response_data = '';

  var options = {
    host: 'api.evercam.io',
    port: 443,
    method: 'POST',
    path: '/v1/cameras?api_id='+process.env['EVERCAM_API_ID']+'&api_key='+process.env['EVERCAM_API_KEY'],
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': camera.length
    }
  };

  console.log(options);

  var request = https.request(options, function(resp){
    resp.on('data', function(chunk){
      response_data=response_data+chunk;
    });
    resp.on('end', function(){
      res.send(response_data);
    });
  }).on("error", function(e){
    console.log("Got error: " + e.message);
  });

  request.end(camera);

});

module.exports = router;
