/* jshint strict: true */
/* global $, jQuery, alert, Mustache, evercam, evercam_auth */
;(function (exports) {
    'use strict';

    var gateways = [];

    /* DOM functions for Gateways */

    // Renders a Gateway as a panel with a show/hide button
    function render(gateway) {
        var template = $("#gateway-template").html(),
            rendered;
        Mustache.parse(template);
        rendered = Mustache.render(template, gateway);
        $("#gateway-list").append(rendered);
    }

   // Binds events specific to a gateway
    function bind(gateway) {
        $("#show-gateway-button" + gateway.id).on("click", function () {
            $("#devices-gateway" + gateway.id).toggle();
            $(this).text(($(this).text() === "## Show") ? ("## Hide") : ("## Show"));
        });
    }

    function render_webrtc_button(gateway, device) {
      return render_webrtc(gateway.id, device.id);
    }

    function render_device_button(gateway, device) {

        if (!device_has_http_or_rtsp(device)) { return ""; }
        var connected = device_status(device).connected;
        if (connected === true) {
            bind_disconnect(gateway.id, device.id);
            return render_disconnect(gateway.id, device.id);
        }
        bind_connect(gateway.id, device.id);
        return render_connect(gateway.id, device.id);
    }

    // Renders the list of devices for each gateway as a DataTable
    function render_devices(gateway) {
       var table_container = $("#devices-table-gateway" + gateway.id);
       var devices_table;
       if (!$.fn.dataTable.isDataTable(table_container)) {
         devices_table =table_container.DataTable({
            "paging":false,
            "ordering": false,
            "info": false,
            "searching":false,
            "columnDefs": [
            {
              targets: [0],
              className: 'text-center' //Center align the image column only
            }],
            "columns": [
              {},
              {"data": undefined, "defaultContent":""},
              {"data": undefined, "defaultContent":""},
              {"data": undefined, "defaultContent":""},
              {"data": undefined, "defaultContent":""},
              {"data": undefined, "defaultContent":""},
              {"data": undefined, "defaultContent":""},
            ]
          });
        }else{
          devices_table = table_container.DataTable();
        }
        devices_table.clear();
        if (gateway.hasOwnProperty('devices')) {
            gateway.devices.forEach(function (device) {
                var vendorId = "";
                if(device.vendor_id !== null) {
                  vendorId = device.vendor_id.toUpperCase();
                }

                //TODO: Hard coding here. This layout should be redesign later
                if(vendorId.length > 10)
                {
                  vendorId = vendorId.substring(0, 10) + "...";
                }

                var thumbnailUrl = device.model_thumbnail_url;
                var thumbnailHtml = "";
                if(thumbnailUrl === null || thumbnailUrl === "") {
                  thumbnailUrl = "http://placehold.it/50/ffffff/ffffff"
                }
                thumbnailHtml = "<img src=" + thumbnailUrl+ " height=\"50\">";
                devices_table.row.add([
                    render_bind_ports_button(gateway.id, device.id, thumbnailHtml),
                    device.ip_address,
                    device.mac_address,
                    vendorId,
                    device.evercam_id,
                    render_device_button(gateway, device),
                    render_webrtc_button(gateway, device)
                ]).draw();
            });
        }
    }

    // Renders the entire list of gateways
    function render_list() {
        $("#gateway-list").html('');
        gateways.forEach(function (gateway) {
            render(gateway);
            render_devices(gateway);
            bind(gateway);
        });
    }

    function render_disconnect(gateway_id, device_id) {
        return "<a class='btn btn-default btn-sm' id='disconnect-" + gateway_id + "-" + device_id + "'>Disconnect</a>";
    }

    function bind_disconnect(gateway_id, device_id) {
        var button_selector = "#disconnect-" + gateway_id + "-" + device_id;
        $("#devices-table-gateway" + gateway_id).off("click", button_selector).on("click", button_selector, function () {
            remove_all_forwarding(gateway_id, device_id);
        });
    }

    function render_connect(gateway_id, device_id) {
        return "<a class='btn btn-default btn-sm' id='connect-" + gateway_id + "-" + device_id + "'>Connect</a>";
    }

    function render_webrtc(gateway_id, device_id) {
      return "<a class='btn btn-default btn-sm' href='play.html' target='_blank' id='play-" + gateway_id + "-" + device_id + "'>Play</a>";
    }

    function bind_connect(gateway_id, device_id) {
        var button_selector = "#connect-" + gateway_id + "-" + device_id;
        $("#devices-table-gateway" + gateway_id).off("click", button_selector).on("click", button_selector, function () {
            forward_http_and_rtsp(gateway_id, device_id);
        });
    }

    function render_bind_ports_button(gateway_id, device_id, content) {
        var button_selector = "show-ports-" + gateway_id + "-" + device_id;
        $("#devices-table-gateway" + gateway_id).off("click", "#" + button_selector).on("click", "#" + button_selector,
            function () {
                show_ports(gateway_id, device_id);
            });
        return "<a id='" + button_selector + "'>"+content+"</a>";
    }

    function render_ports(device, gateway) {
        var ports_table = $("#ports-table-device" + device.id).DataTable(
                              {"columns": [
                                  {},
                                  {"data": undefined, "defaultContent":""},
                                  {"data": undefined, "defaultContent":""},
                                  {},
                                  {}
                                ],
                                "paging":false,
                                "ordering": false,
                                "info": false,
                                "searching":false
                              });
        ports_table.clear();
        if (device.hasOwnProperty('ports')) {
            device.ports.forEach(function (port) {
                ports_table.row.add([
                    port.port_id,
                    port.local_port_id,
                    port.public_port_id,
                    (function () {
                        return port.bind_public_ip ? gateway.vpn_hostname : "N/A";
                    }()),
                    port.service,
                ]).draw();
            });
        }
    }

    function show_ports(gateway_id, device_id) {
        var device = get_device(gateway_id, device_id),
            gateway = get_gateway(gateway_id),
            template = $("#ports-template").html(),
            rendered;
        Mustache.parse(template);
        rendered = Mustache.render(template, device);
        $('body').append(rendered);
        render_ports(device, gateway);
        // Bind handler that removes Modal from DOM when it is dismissed
        $("#ports-device" + device_id).on("hidden.bs.modal", function () {
            $(this).remove();
        });
        $("#ports-device" + device_id).modal('show');
    }

    /* Add a Camera / Evercam related functions */

    function validate_camera(device_id) {
        //not implemented
        return true;
    }

    function evercam_add_camera(device_id, gateway_id) {
        if (validate_camera) {
            var gateway = get_gateway(gateway_id),
                device = get_device(gateway_id, device_id),
                http_port = get_first_http_port(device),
                rtsp_port = get_first_rtsp_port(device),
                camera = {
                    name: device.vendor_id+" "+device.model_id,
                    vendor: device.vendor_id,
                    model: device.model_id,
                    jpg_url: device.http_jpg_path,
                    h264_url: device.rtsp_h264_path,
                    cam_username: device.default_username,
                    cam_password: device.default_password,
                    is_public: false,
                    mac_address: device.mac_address,
                    external_host: gateway.vpn_hostname,
                    internal_host: gateway.ip_address,
                    external_http_port: http_port.public_port_id.toString(),
                    internal_http_port: http_port.local_port_id.toString(),
                    external_rtsp_port: rtsp_port.public_port_id.toString(),
                    internal_rtsp_port: rtsp_port.local_port_id.toString()
                };
                var mandatory = ['is_public'];
				for (var property in camera) {
				    if (!mandatory.indexOf(property)) continue;
					if (camera.hasOwnProperty(property)) {
						// remove empty ones
                        if (!camera[property]) {
                          console.log(camera[property]);
                          delete camera[property];
                        }
					}
				}
               console.log(camera);
               var evercamToken = evercam_auth.token();
               evercam.add_camera(camera, evercamToken, function (data) {
                  console.log(data);
                  if (data.hasOwnProperty('cameras')) {
                    // save the Evercam ID back to the device
                    var device_update = {id: device_id, evercam_id: data.cameras[0].id};
                    evercam_gateway.update_device(gateway_id, device_update,evercamToken.access_token,
                                        function() {
                                           console.log("Added Evercam ID to Device. Maybe...");
                                           //set gateway device property
                                           device.evercam_id = data.cameras[0].id;
                                           // rerender device data
                                           render_devices(gateway);
                                        });

                    // go to camera id (always only one in this situation)
                    window.open('https://dash.evercam.io/v1/cameras/' + data.cameras[0].id  + '/live', '_blank');
                }
            });
        }
    }

    /* Gateway related functions */

    // returns a specific gateway from the gateways array
    function get_gateway(gateway_id) {
        var result;
        gateways.forEach(function (gateway) {
            if (gateway.id === gateway_id) {
                result = gateway;
            }
        });
        return result;
    }

    // Another fun function. Finds any HTTP and RTSP ports on a device
    // and forwards them
    function forward_http_and_rtsp(gateway_id, device_id) {
        var gateway = get_gateway(gateway_id);
        if (gateway.hasOwnProperty('devices')) {
            gateway.devices.forEach(function (device) {
                if (device.id === device_id) {
                    var all_candidate_ports = all_http_and_rtsp(device.ports),
                    last_port_id = all_candidate_ports[all_candidate_ports.length - 1].port_id;
                    var rules = { device_id: device_id, rules: [] };
                    all_candidate_ports.forEach(function (port) {
                        // Define list of rules for sending
                        var rule = {
                            "device_id": device.id,
                            "port_id": port.port_id,
                            "bind_public_ip": true
                        };
                        rules.rules.push(rule);
                    });
                    evercam_gateway.add_rule(gateway.id, rules, evercam_auth.access_token(),
                            function (data) {
                                console.log("Rules Added!");

                                data.rules.forEach(function(confirmed_rule) {
                                  if (confirmed_rule.hasOwnProperty('port_id')) {
                                    var result = $.grep(device.ports, function(e) { return e.port_id===confirmed_rule.port_id; });
                                    update_port(result[0], confirmed_rule);
                                  }
                                });

                                render_devices(gateway);
                                // And now add the camera to Evercam
                                evercam_add_camera(device.id, gateway.id);
                    });
                }
            });
        }
    }

    function remove_all_forwarding(gateway_id, device_id) {
        var device = get_device(gateway_id, device_id);
        if (device_status(device).connected) {
            device.ports.forEach(function (port) {
                if (port.hasOwnProperty('local_port_id')) {
                    var rule = {
                        device_id: device_id,
                        port_id: port.port_id
                    };
                    console.log(rule);
                    var evercamToken = evercam_auth.access_token();
                    evercam_gateway.delete_rule(gateway_id, rule, evercamToken,
                        function () {
                            var gateway = get_gateway(gateway_id);
                            console.log("Rule deleted");
                            update_port(port, undefined);

                            // now delete the evercam_id from device
                            // save the Evercam ID back to the device
                            var device_update = {id: device_id, evercam_id: ""};
                            evercam_gateway.update_device(gateway_id, device_update,evercamToken,
                                        function() {
                                           console.log("Removed Evercam ID from Device.");
                                           //set gateway device property
                                           device.evercam_id = "";
                                           // rerender device data
                                           render_devices(gateway);
                                        });


                        });
                }
            });
        }
    }

    /* Device related functions */

    // Crazy name - crazy function! This checks if the device has any
    // HTTP or RTSP ports. Since we're currently keeping forwarding to those
    // only, this allows us to filter on devices that are valid
    function device_has_http_or_rtsp(device) {
        var result = false;
        device.ports.forEach(function (port) {
            if (!result) { result = is_http_or_rtsp(port); }
        });
        return result;
    }

    // gets a specific device object from owner gateway in gateways array
    function get_device(gateway_id, device_id) {
        var gateway = get_gateway(gateway_id),
            result;
        if (gateway.hasOwnProperty('devices')) {
            gateway.devices.forEach(function (device) {
                if (device.id === device_id) {
                    result = device;
                }
            });
        }
        return result;
    }

    function device_status(device) {
        var connected = {
            connected: false,
            message: "Not Connected"
        };
        device.ports.forEach(function (port) {
            // If any of the ports has a local_port_id (a gateway port)
            // then the device is forwarded (at least in part). TODO: For now
            // we're only forwarding ports HTTP and RTSP and we're not
            // giving fine-grained control over it. Either they're all forwarded
            // or none. Later on, we'll agree an advanced interface for specific
            // port forwarding
            if (port.hasOwnProperty('local_port_id')) {
                connected = {
                    connected: true,
                    message: "Connected"
                };
            }
        });
        return connected;
    }

    function get_first_http_port(device) {
        var result = {
            local_port_id: "",
            public_port_id: ""
        };
        device.ports.sort(port_compare_desc).forEach(function (port) {
            console.log(port);
            if (is_http(port) && result.local_port_id === "") {
                result = port;
            }
        });
        return result;
    }

    function get_first_rtsp_port(device) {
        var result = {
            local_port_id: "",
            public_port_id: ""
        };
        device.ports.sort(port_compare_desc).forEach(function (port) {
            if (is_rtsp(port) && result.local_port_id === "") {
                result = port;
            }
        });
        return result;
    }

    /* Port related functions */

    // checks if a port is forwarded
    function is_forwarded(element, index, array) {
      return element.hasOwnProperty('local_port_id');
    }

    // takes a list of ports and returns those with http or rtsp services
    function all_http_and_rtsp(ports) {
        var result = [];
        ports.forEach(function (port) {
            if (is_http_or_rtsp(port)) {
                result.push(port);
            }
        });
        return result;
    }

    // Sets or deletes forwarding information for a
    // specific port object. Works on object within gateways array
    // and mutates it in place.
    function update_port(port, data) {
        if (data !== undefined) {
            port.local_port_id = data.local_port_id;
            port.public_port_id = data.public_port_id;
            port.bind_public_ip = data.bind_public_ip;
        } else {
            delete port.local_port_id;
            delete port.public_port_id;
            delete port.bind_public_ip;
        }
    }

    // Comparison function for ordering ports by
    // Port number, descending
    function port_compare_desc(a, b) {
        if (a.port_id > b.port_id) {
            return 1;
        }
        if (a.port_id < b.port_id) {
            return -1;
        }
        return 0;
    }

    // Checks if a port provides either an RTSP or HTTP service
    function is_http_or_rtsp(port) {
        if (is_http(port) || is_rtsp(port)) {
            return true;
        }
        return false;
    }

    // Checks if a port provides an HTTP based service
    function is_http(port) {
        if (port.service === 'http' || port.service === 'HTTP') {
            return true;
        }
        return false;
    }

    // Checks if a port provides an RTSP based service
    function is_rtsp(port) {
        if (port.service === 'rtsp' || port.service === 'RTSP') {
            return true;
        }
        return false;
    }

    /* Exportable object */

    var gateway_list = {

        add: function (gateway) {
            console.log(gateway);
            gateways.push(gateway);
            render(gateway);
            render_devices(gateway);
            bind(gateway);
        }

    };

    exports.gateway_list = gateway_list;

}(this));
