/*jslint browser: true*/
/*global $, jQuery, alert*/
;(function (exports) {
    'use strict';

    var api_url = "https://api.gateway.evercam.io:443/v1";

    function validate_claim(mac_address, secret) {
        // Not yet implemented - still awaiting word on which validation library to use
        return true;
    }

    var evercam_gateway = {

        claim_gateway: function (mac_address, secret, name, onCompletion) {
            if (!validate_claim(mac_address, secret)) {
                return;
            }
            var url = api_url + "/gateways/" + mac_address + "?secret=" + secret;
            $.getJSON(url, function (data) {
                // add the secret to the object so it can be passed to approval easily
                data.secret = secret;
                if(name == '') {
                  // give the pending gateway a generic name
                  data.gatewayName = "My Little Gateway";
                }
                else {
                  data.gatewayName = name;
                }

                onCompletion(data);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
                onCompletion(undefined);
            });
        },

        approve_gateway: function (pendingGateway, evercamToken, onCompletion) {
            console.log("Attempting to approve Gateway...");
            var url = api_url + "/gateways/" + pendingGateway.mac_address,
                approval = {
                    secret: pendingGateway.secret,
                    name: pendingGateway.gatewayName,
                    evercam_token: evercamToken
                };
            $.ajax({
                type: 'PATCH',
                url: url,
                data: JSON.stringify(approval),
                processData: false,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    onCompletion(data);
                }
            });
        },

        get_gateways: function (evercamToken, onCompletion) {
            console.log("Getting list of gateways owned by this user...");
            var url = api_url + "/gateways/?evercam_token=" + evercamToken;
            $.getJSON(url, function (data) {
                onCompletion(data);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
                onCompletion(undefined);
            });
        },

        get_devices: function (evercamToken, gateway_id, onCompletion) {
            console.log("Getting devices attached to gateway...");
            var url = api_url + "/gateways/" + gateway_id + "/devices?evercam_token=" + evercamToken;
            $.getJSON(url, function (data) {
                onCompletion(data);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
                onCompletion(undefined);
            });
        },

        add_rule: function (gateway_id, rule, evercamToken, onCompletion) {
            console.log("Adding a forwarding rule...");
            var url = api_url + "/gateways/" + gateway_id + "/rules?evercam_token=" + evercamToken;
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(rule),
                processData: false,
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    onCompletion(data);
                }
            });
        },

        delete_rule: function (gateway_id, rule, evercamToken, onCompletion) {
            console.log("Removing a forwarding rule...");
            var url = api_url + "/gateways/" + gateway_id + "/rules?device_id=" + rule.device_id + "&port_id=" + rule.port_id + "&evercam_token=" + evercamToken;
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function (data) {
                    onCompletion(data);
                }
            });
        },

        update_device: function(gateway_id, device, evercamToken, onCompletion) {
          console.log("Updating device...");
          var url = api_url + "/gateways/" + gateway_id + "/devices/" + device.id + "?evercam_token=" + evercamToken;
          $.ajax({
            type: 'PATCH',
            url: url,
            data: JSON.stringify(device),
            processData: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                onCompletion(data);
            }
          });

        }

    };

    exports.evercam_gateway = evercam_gateway;

}(this));
